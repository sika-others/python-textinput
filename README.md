PyTextInput
===========

Simple vim text input for Python

### Authors
*  Ondrej Sika, <http://ondrejsika.com>, dev@ondrejsika.com

### Source
* Python Package Index: <http://pypi.python.org/pypi/pytextinput>
* GitHub: <https://github.com/sikaondrej/python-textinput>


Documentation
-------------

### Instalation
Instalation is very simple via pip.

    pip install pytextinput

### Usage

    from textinput import textinput, viminput
    # use vim
    text = viminput()
    print text
    # use $EDITOR
    text = textinput()
    # set editor
    text = textinput(editor="nano")
    # default data
    text = textinput(content="default data")