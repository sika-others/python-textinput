#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name = "pytextinput",
    version = "1.0.1",
    url = 'https://github.com/sikaondrej/python-textinput',
    license = 'GNU LGPL v.3',
    description = "Text input for Python",
    author = 'Ondrej Sika',
    author_email = 'ondrej@ondrejsika.com',
    packages = [],
    py_modules = ["textinput", ],
    requires = [],
    include_package_data = True,
    zip_safe = False,
)
